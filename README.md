## Simple app

App configuration 
- you need to provide username and password in application.yml, look at provided example.
- you need to provide ticketing service endpoint in application.yml and have respective service running on declared url and port
- it is expected that ticketing service will return status code 0 as success

App Adapter provides two capabilities:
1. simple 
   - with 3 parameters (barcode, status, operation) 
     - supported operations are 'update' and 'create'.
     - status represents state of the given PC in ticketing service.
   - this endpoints load all necessary information from PC, transforms it to ticket message and sends to the endpoint.
2. bulk
   - with 3 parameters (list of barcode, status, operation)
   - works same as endpoint above, just this one is able to process and create multiple tickets in parallel
3. raw
   - simple endpoint with one parameter message of type json. This message will be resent to ticketing service endpoint without
   any validation.
