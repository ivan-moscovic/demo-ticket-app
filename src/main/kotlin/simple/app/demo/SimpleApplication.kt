package simple.app.demo

import simple.app.demo.apollo.PcServiceProperties
import io.ktor.client.engine.*
import io.ktor.client.engine.cio.*
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import simple.app.demo.apollo.ApolloClient

fun main(args: Array<String>) {
  runApplication<SimpleApplication>(*args)
}

@SpringBootApplication
@ConfigurationPropertiesScan
class SimpleApplication {

  @Bean
  fun ticketHttpClientEngine() = CIO.create()

  @Bean
  fun apolloHttpClientEngine() = CIO.create()

  @Bean
  fun apolloClient(apolloProperties: PcServiceProperties, apolloHttpClientEngine: HttpClientEngine) = ApolloClient(apolloProperties.url, apolloHttpClientEngine)
}
