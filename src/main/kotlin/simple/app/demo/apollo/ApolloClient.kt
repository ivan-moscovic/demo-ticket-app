package simple.app.demo.apollo

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.module.kotlin.readValue
import simple.app.demo.serializers.Serializers
import io.ktor.client.*
import io.ktor.client.engine.*
import io.ktor.client.features.logging.*
import io.ktor.client.request.*
import io.ktor.http.*
import io.ktor.http.content.*
import kotlin.reflect.KClass

class ApolloClient(
  private val apolloUrl: String,
  apolloHttpClientEngine: HttpClientEngine
) {

  private val httpClient = HttpClient(apolloHttpClientEngine) {
    install(Logging) {
      logger = Logger.DEFAULT
      level = LogLevel.BODY
    }
  }

  suspend fun <R : Any> queryData(query: String, klass: KClass<R>, variables: Map<String, *> = mapOf<String, Any>()): R {
    val result = query(query, variables)
    check(result.errors.isNullOrEmpty()) { "Error calling apollo client. ${result.errors}" }
    return Serializers.json.convertValue(result.data!!, klass.java)
  }

  private suspend fun query(query: String, variables: Map<String, *> = mapOf<String, Any>()): GraphQLResponse<JsonNode> {
    val rawResult = httpClient.post<String>(apolloUrl) {
      body = TextContent(Serializers.json.writeValueAsString(GraphQLRequest(variables, query)), ContentType.Application.Json)
    }
    return Serializers.json.readValue(rawResult)
  }

}

private data class GraphQLRequest(
  val variables: Map<String, *>,
  val query: String
)

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
data class GraphQLResponse<T>(
  val data: T? = null,
  val errors: List<GraphQLError>? = null
)

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
data class GraphQLError(
  val message: String
)
