package simple.app.demo.apollo

import org.springframework.boot.context.properties.ConfigurationProperties

@ConfigurationProperties(prefix = "simple-app.pc-service-endpoint")
data class PcServiceProperties(
  val url: String
)
