package simple.app.demo.apollo

import org.springframework.stereotype.Component

@Component
class PcSupplier(
  private val apolloClient: ApolloClient
) {

  private val query = this::class.java.getResourceAsStream("/queries/getPCQuery.graphql")!!.reader().use { it.readText() }
  suspend fun getPc(pcBarcode: String): GetPcResult.PersonalComputer? {
    val pc = apolloClient.queryData(query, GetPcResult::class, mapOf("pcBarcode" to pcBarcode))
    return pc.instances.first()
  }
}

data class GetPcResult(
  val instances: List<PersonalComputer>
) {

  data class PersonalComputer(
    val internalId: String,
    val serialNumber: StringAttribute,
    val barcode: StringAttribute,
    val firmware: StringAttribute,
    val brandId: StringAttribute,
    val configuration: StringAttribute,
    val otherComponents: List<Component>
  )

  data class Component(
    val internalId: String,
    val componentName: StringAttribute,
    val additionalInfos: List<AdditionalInfo>
  ) {
    val additionalInfo = additionalInfos.firstOrNull()
  }

  data class AdditionalInfo(
    val existsFrom: String?,
  )

  data class StringAttribute(
    val normalizedValue: String?
  )
}