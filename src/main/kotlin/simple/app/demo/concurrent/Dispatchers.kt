package simple.app.demo.concurrent

import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.Dispatchers
import mu.KotlinLogging

private val log = KotlinLogging.logger {}

private val coroutineExceptionHandler = CoroutineExceptionHandler { _, exception -> log.error(exception) { "Unhandled exception" } }

val ioDispatcher = Dispatchers.IO + coroutineExceptionHandler
