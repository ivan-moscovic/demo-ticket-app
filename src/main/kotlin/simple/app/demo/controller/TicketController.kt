package simple.app.demo.controller

import com.fasterxml.jackson.databind.JsonNode
import jakarta.validation.Valid
import kotlinx.coroutines.*
import simple.app.demo.apollo.PcSupplier
import simple.app.demo.apollo.GetPcResult
import simple.app.demo.utils.time.now
import mu.KotlinLogging
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import simple.app.demo.Constants
import simple.app.demo.concurrent.ioDispatcher
import simple.app.demo.service.*
import java.time.Instant

@RestController
class TicketController(
  private val ticketService: TicketService,
  private val pcSupplier: PcSupplier
) {
  private val log = KotlinLogging.logger { }

  @PostMapping("capability/simple")
  suspend fun simpleRequest(@Valid @RequestBody request: ExecuteActionSchemaSimple): ActionResultSchema {
    return processRequest(request) { startTime ->
      val pc = pcSupplier.getPc(request.barcode) ?: error("PC with barcode=${request.barcode} was not found.")
      processPC(request, pc, startTime)
    }
  }

  @PostMapping("capability/bulk")
  suspend fun bulkRequest(@Valid @RequestBody request: ExecuteActionSchema): ActionResultSchema {
    return processRequest(request) { startTime ->
      val deferredResults = request.barcode.map { barcode ->
        CoroutineScope(ioDispatcher).async {
          val pc = pcSupplier.getPc(barcode) ?: error("PC with barcode=${request.barcode} was not found.")
          processPC(
            ExecuteActionSchemaSimple(
              executionId = request.executionId,
              barcode = barcode,
              operation = request.operation,
              status = request.status
            ), pc, startTime
          )
        }
      }
      val results = deferredResults.awaitAll()
      if (results.all { it.resultStatus == Constants.ResultStatus.Ok }) {
        ActionResultSchema(
          executionId = request.executionId,
          activeFrom = startTime.toString(),
          activeTo = now(),
          resultStatus = Constants.ResultStatus.Ok
        )
      } else {
        ActionResultSchema(
          executionId = request.executionId,
          activeFrom = startTime.toString(),
          activeTo = now(),
          resultStatus = Constants.ResultStatus.nOk,
          errorMessage = "Bulk request has failed: ${results.filter { it.resultStatus == Constants.ResultStatus.nOk }.map { it.errorMessage }.joinToString()}"
        )
      }
    }
  }

  private suspend fun processPC(
    request: ExecuteActionSchemaSimple,
    pc: GetPcResult.PersonalComputer,
    startTime: Instant
  ): ActionResultSchema {
    log.debug { "PC with barcode=${request.barcode} was found, PC: $pc" }
    check(pc.otherComponents.all { it.componentName.normalizedValue != null }) { "Missing other connected component name" }

    val pcTicket = pc.toPcTicket(request.operation, request.status, startTime)

    val response = ticketService.sendRequest(pcTicket)
    return processResponse(request, response, startTime)
  }

  @PostMapping("capability/raw")
  suspend fun rawRequest(@Valid @RequestBody request: ExecuteActionSchemaRaw): ActionResultSchema {
    return processRequest(request) {
      val response = ticketService.sendRequest(request.rawValue)
      processResponse(request, response, it)
    }
  }

  private suspend fun processRequest(request: ExecutionActionSchema, action: suspend (startTime: Instant) -> ActionResultSchema): ActionResultSchema {
    val startTime = now()
    return try {
      action(startTime)
    } catch (t: Throwable) {
      log.error(t) { "Exception while executing capability" }
      val errorMessage = t.message ?: t.toString()
      processFail(errorMessage, request, startTime)
    }
  }

  private suspend fun processResponse(request: ExecutionActionSchema, response: TicketResponse, startTime: Instant): ActionResultSchema {
    log.debug { "Response from ticketing service is: $response" }
    return if (response.code != 0) {
      val errorMessage = "Request to ticketing service has failed. Returned error code:'${response.code}' and error message:'${response.message}'."
      log.error { errorMessage }
      processFail(errorMessage, request, startTime)
    } else {
      ActionResultSchema(
        executionId = request.executionId,
        activeFrom = startTime.toString(),
        activeTo = now(),
        resultStatus = Constants.ResultStatus.Ok
      )
    }
  }

  private suspend fun processFail(
    errorMessage: String,
    request: ExecutionActionSchema,
    startTime: Instant
  ): ActionResultSchema {
    return ActionResultSchema(
      executionId = request.executionId,
      activeFrom = startTime.toString(),
      activeTo = now(),
      resultStatus = Constants.ResultStatus.nOk,
      errorMessage = errorMessage
    )
  }


  fun GetPcResult.PersonalComputer.toPcTicket(operation: TicketOperation, status: String, timestamp: Instant) = Ticket(
    operation = operation,
    timestamp = timestamp.toString(),
    output_fields = "id",
    clazz = "PC",
    comment = "Synchronization from API",
    fields = TicketFields(
      barcode = this.barcode.normalizedValue,
      serialNumber = this.serialNumber.normalizedValue,
      status = status,
      brand_id = Named(this.brandId.normalizedValue),
      version_firmware = Named(this.firmware.normalizedValue),
      version_configuration = Named(this.configuration.normalizedValue),
      other_connected_components = this.otherComponents.map { Component(it.componentName.normalizedValue!!, it.additionalInfo?.existsFrom) }
    )
  )
}

@Suppress("unused")
class ActionResultSchema(
  val executionId: String,
  val activeFrom: String,
  val activeTo: Instant,
  val resultStatus: String,
  val errorMessage: String? = null
)

interface ExecutionActionSchema {
  val executionId: String
}

data class ExecuteActionSchemaSimple(
  override val executionId: String,
  val barcode: String,
  val operation: TicketOperation,
  val status: String
) : ExecutionActionSchema

data class ExecuteActionSchema(
  override val executionId: String,
  val barcode: List<String>,
  val operation: TicketOperation,
  val status: String
) : ExecutionActionSchema

data class ExecuteActionSchemaRaw(
  override val executionId: String,
  val rawValue: JsonNode
) : ExecutionActionSchema
