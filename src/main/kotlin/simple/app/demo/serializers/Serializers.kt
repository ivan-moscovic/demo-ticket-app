package simple.app.demo.serializers

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.JsonNodeFactory
import com.fasterxml.jackson.databind.node.NullNode
import com.fasterxml.jackson.databind.node.ValueNode
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.registerKotlinModule


object Serializers {
  val json = ObjectMapper()
    .registerModule(JavaTimeModule())
    .registerKotlinModule()
}

fun nullNode(): NullNode = NullNode.getInstance()
fun Any?.toJsonNode(): ValueNode = if (this != null) JsonNodeFactory.instance.pojoNode(this) else nullNode()
