package simple.app.demo.service

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import simple.app.demo.utils.time.ClockUtils
import java.time.Instant
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter

@JsonInclude(JsonInclude.Include.NON_NULL)
data class Ticket(
  val operation: TicketOperation,
  val timestamp: String,
  val output_fields: String,
  @JsonProperty("class")
  val clazz: String?,
  val comment: String?,
  val fields: TicketFields
)

@JsonInclude(JsonInclude.Include.NON_NULL)
data class TicketFields(
  val barcode: String?,
  val serialNumber: String?,
  val status: String?,
  @JsonInclude(value = JsonInclude.Include.CUSTOM, valueFilter = Named::class)
  val brand_id: Named?,
  @JsonInclude(value = JsonInclude.Include.CUSTOM, valueFilter = Named::class)
  val version_firmware: Named?,
  @JsonInclude(value = JsonInclude.Include.CUSTOM, valueFilter = Named::class)
  val version_configuration: Named?,
  val other_connected_components: List<Component>
)

@JsonInclude(JsonInclude.Include.NON_NULL)
data class Named(
  val name: String? = null
)

@JsonInclude(JsonInclude.Include.NON_NULL)
data class Component(
  val name: String,
  val existsFrom: String?
)

enum class TicketOperation {
  @JsonProperty("core/create")
  CREATE,
  @JsonProperty("core/update")
  UPDATE
}

data class TicketResponse(
  val code: Int,
  val message: String
)