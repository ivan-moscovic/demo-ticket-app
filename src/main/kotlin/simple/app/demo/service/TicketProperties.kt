package simple.app.demo.service

import org.springframework.boot.context.properties.ConfigurationProperties

@ConfigurationProperties(prefix = "simple-app.ticket-endpoint")
data class TicketProperties(
  val url: String,
  val version: String,
  val userName: String,
  val userPassword: String
)
