package simple.app.demo.service

import com.fasterxml.jackson.databind.JsonNode
import io.ktor.client.*
import io.ktor.client.engine.*
import io.ktor.client.features.json.*
import io.ktor.client.features.logging.*
import io.ktor.client.request.*
import io.ktor.http.*
import org.springframework.stereotype.Component
import simple.app.demo.serializers.Serializers
import simple.app.demo.serializers.toJsonNode

@Component
class TicketService(
  private val ticketProperties: TicketProperties,
  ticketHttpClientEngine: HttpClientEngine
) {
  private val ticketingServiceHttpClient: HttpClient = HttpClient(ticketHttpClientEngine) {
    install(Logging) {
      logger = Logger.DEFAULT
      level = LogLevel.INFO
    }
    install(JsonFeature) {
      serializer = JacksonSerializer(Serializers.json)
    }
  }

  suspend fun sendRequest(ticket: Ticket): TicketResponse {
    return sendRequest(ticket.toJsonNode())
  }

  suspend fun sendRequest(serializedTicket: JsonNode): TicketResponse {
    return ticketingServiceHttpClient.post {
      url(
        "${ticketProperties.url}?auth_user=${ticketProperties.userName}&auth_pwd=${ticketProperties.userPassword}" +
                "&version=${ticketProperties.version}"
      )
      body = serializedTicket
      contentType(ContentType.Application.Json)
    }
  }
}