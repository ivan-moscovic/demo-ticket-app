package simple.app.demo.utils.time

import java.time.Clock
import java.time.Instant
import java.time.ZoneId

object ClockUtils {
  val PRAGUE_ZONE: ZoneId = ZoneId.of("Europe/Prague")

  @Volatile
  var CLOCK: Clock = Clock.system(PRAGUE_ZONE)

  fun setFixedClock(instant: Instant, zoneId: ZoneId = PRAGUE_ZONE) {
    CLOCK = Clock.fixed(instant, zoneId)
  }
}

fun now(): Instant = Instant.now(ClockUtils.CLOCK)
