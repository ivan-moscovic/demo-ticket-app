package simple.app.demo

import simple.app.demo.ktormock.KtorClientMockEngine
import simple.app.demo.serializers.Serializers
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.test.web.server.LocalServerPort
import org.springframework.http.*
import org.springframework.test.context.TestPropertySource
import org.springframework.http.HttpEntity
import simple.app.demo.controller.*
import simple.app.demo.service.TicketOperation

@SpringBootTest(
  classes = [SimpleApplication::class, TestContextConfiguration::class],
  webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
@TestPropertySource("classpath:application.yaml")
abstract class AbstractContextTest {

  @LocalServerPort
  private val port = 0

  @Autowired
  private lateinit var restTemplate: TestRestTemplate

  @Autowired
  protected lateinit var apolloHttpClientEngine: KtorClientMockEngine

  @Autowired
  protected lateinit var ticketHttpClientEngine: KtorClientMockEngine

  @BeforeEach
  @AfterEach
  fun reset() {
    apolloHttpClientEngine.reset()
    ticketHttpClientEngine.reset()
  }

  fun sendTicketSimpleRequest(barcode: String, operation: TicketOperation, status: String): ResponseEntity<ActionResultSchema> {
    return sendRequest("http://localhost:$port/capability/simple", createRequest(barcode, operation, status))
  }

  fun sendTicketBulkRequest(barcodes: List<String>, operation: TicketOperation, status: String): ResponseEntity<ActionResultSchema> {
    return sendRequest("http://localhost:$port/capability/bulk", createRequest(barcodes, operation, status))
  }

  fun sendTicketRawRequest(message: String): ResponseEntity<ActionResultSchema> {
    return sendRequest("http://localhost:$port/capability/raw", createRequest(message))
  }

  fun assertOkResponse(response: ResponseEntity<ActionResultSchema>) {
    assertThat(response.statusCode).isEqualTo(HttpStatus.OK)
    assertThat(response.body!!.resultStatus).isEqualTo(Constants.ResultStatus.Ok)
  }

  fun assertNokResponse(response: ResponseEntity<ActionResultSchema>, errorMessage: String) {
    assertThat(response.statusCode).isEqualTo(HttpStatus.OK)
    assertThat(response.body!!.resultStatus).isEqualTo(Constants.ResultStatus.nOk)
    assertThat(response.body!!.errorMessage).isEqualTo(errorMessage)
  }


  private fun sendRequest(url: String, body: ExecutionActionSchema): ResponseEntity<ActionResultSchema> {
    val headers = HttpHeaders().apply {
      contentType = MediaType.APPLICATION_JSON
    }
    return restTemplate.postForEntity(
      url,
      HttpEntity(body, headers),
      ActionResultSchema::class.java
    )
  }

  private fun createRequest(message: String): ExecutionActionSchema {
    return ExecuteActionSchemaRaw("executionNodeId", Serializers.json.readTree(message))
  }

  private fun createRequest(barcode: String, operation: TicketOperation, status: String): ExecutionActionSchema {
    return ExecuteActionSchemaSimple(
      executionId = "executionNodeId",
      barcode = barcode,
      operation = operation,
      status = status
    )
  }

  private fun createRequest(barcodes: List<String>, operation: TicketOperation, status: String): ExecutionActionSchema {
    return ExecuteActionSchema(
      executionId = "executionNodeId",
      barcode = barcodes,
      operation = operation,
      status = status
    )
  }

  fun readResource(path: String): String {
    return this::class.java.getResourceAsStream(path)!!.reader().use {
      it.readText()
    }
  }
}