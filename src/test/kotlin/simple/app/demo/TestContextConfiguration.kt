package simple.app.demo

import simple.app.demo.ktormock.KtorClientMockEngine
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class TestContextConfiguration {

  @Bean
  fun apolloHttpClientEngine() = KtorClientMockEngine()

  @Bean
  fun ticketHttpClientEngine() = KtorClientMockEngine()

}
