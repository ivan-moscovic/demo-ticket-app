package simple.app.demo.capabilities

import simple.app.demo.AbstractContextTest
import simple.app.demo.service.TicketResponse
import simple.app.demo.utils.time.ClockUtils
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Test
import simple.app.demo.ktormock.*
import simple.app.demo.service.TicketOperation
import java.time.Instant

class TicketCapabilitiesTest : AbstractContextTest() {

  @Test
  fun `test simple request create ticket`(): Unit = runBlocking {
    val fixedTime = Instant.parse("2022-01-21T07:30:00Z")
    ClockUtils.setFixedClock(fixedTime)

    apolloHttpClientEngine.expectGraphQlVariables(mapOf("pcBarcode" to "barcode100"))
      .andRespond(readResource("/getPCResponse.json"))

    prepareTicketingServiceMock(
      "/sendTicketCreateMessage.json",
      TicketResponse(
        code = 0,
        message = "Ticket created!"
      )
    )

    val response = sendTicketSimpleRequest("barcode100", TicketOperation.CREATE, "stock")
    assertOkResponse(response)
    apolloHttpClientEngine.verify()
    ticketHttpClientEngine.verify()
  }

  @Test
  fun `test bulk request create ticket`(): Unit = runBlocking {
    val fixedTime = Instant.parse("2022-01-21T07:30:00Z")
    ClockUtils.setFixedClock(fixedTime)

    apolloHttpClientEngine.expectGraphQlVariables(mapOf("pcBarcode" to "barcode100"))
      .andRespond(readResource("/getPCResponse.json"))

    apolloHttpClientEngine.expectGraphQlVariables(mapOf("pcBarcode" to "barcode100"))
      .andRespond(readResource("/getPCResponse.json"))

    prepareTicketingServiceMock(
      "/sendTicketCreateMessage.json",
      TicketResponse(
        code = 0,
        message = "Ticket created!"
      )
    )
    prepareTicketingServiceMock(
      "/sendTicketCreateMessage.json",
      TicketResponse(
        code = 0,
        message = "Ticket created!"
      )
    )

    val response = sendTicketBulkRequest(listOf("barcode100", "barcode100"), TicketOperation.CREATE, "stock")
    assertOkResponse(response)
    apolloHttpClientEngine.verify()
    ticketHttpClientEngine.verify()
  }

  @Test
  fun `test ticket simple update request`(): Unit = runBlocking {
    val fixedTime = Instant.parse("2022-01-21T07:30:00Z")
    ClockUtils.setFixedClock(fixedTime)

    apolloHttpClientEngine.expectGraphQlVariables(mapOf("pcBarcode" to "barcode100"))
      .andRespond(readResource("/getPCResponseWithNulls.json"))
    prepareTicketingServiceMock(
      "/sendTicketUpdateMessage.json",
      TicketResponse(
        code = 0,
        message = "Ticket updated!"
      )
    )

    val response = sendTicketSimpleRequest("barcode100", TicketOperation.UPDATE, "installed")
    assertOkResponse(response)
    apolloHttpClientEngine.verify()
    ticketHttpClientEngine.verify()
  }

  @Test
  fun `test ticket simple request fail`(): Unit = runBlocking {
    val fixedTime = Instant.parse("2022-01-21T07:30:00Z")
    ClockUtils.setFixedClock(fixedTime)

    apolloHttpClientEngine.expectGraphQlVariables(mapOf("pcBarcode" to "barcode100"))
      .andRespond(readResource("/getPCResponse.json"))

    prepareTicketingServiceMock(
      "/sendTicketCreateMessage.json",
      TicketResponse(
        code = 100,
        message = "Fail!"
      )
    )

    val response = sendTicketSimpleRequest("barcode100", TicketOperation.CREATE, "stock")
    assertNokResponse(response, "Request to ticketing service has failed. Returned error code:'100' and error message:'Fail!'.")
    apolloHttpClientEngine.verify()
    ticketHttpClientEngine.verify()
  }

  @Test
  fun `test raw request`(): Unit = runBlocking {
    val request = readResource("/sendTicketUpdateRawMessage.json")

    prepareTicketingServiceMock(
      "/sendTicketUpdateRawMessage.json",
      TicketResponse(
        code = 0,
        message = "Ticket Updated!"
      )
    )
    val response = sendTicketRawRequest(request)
    assertOkResponse(response)
    apolloHttpClientEngine.verify()
    ticketHttpClientEngine.verify()
  }

  private fun prepareTicketingServiceMock(expectedJsonDataString: String, response: TicketResponse) {
    val req = readResource(expectedJsonDataString)
    ticketHttpClientEngine.expect(
      req,
      mapOf(
        "auth_user" to AssertUrlParameter(false, "TestUser"),
        "auth_pwd" to AssertUrlParameter(false, "1234NBS")
      )
    ).andRespondJson(response)
  }
}