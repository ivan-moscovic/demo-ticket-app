package simple.app.demo.ktormock

import io.ktor.client.engine.*
import io.ktor.client.engine.mock.*
import io.ktor.client.request.*
import io.ktor.util.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import java.util.concurrent.CopyOnWriteArrayList

class KtorClientMockEngine : HttpClientEngineBase("KtorClientMockEngine") {

  override val config = HttpClientEngineConfig()
  override val dispatcher = Dispatchers.IO

  private val expectations = CopyOnWriteArrayList<RequestExpectation>()
  private val mutex = Mutex()

  @Volatile
  private var invocationCount = 0

  fun expect(requestMatcher: RequestMatcher): ResponseActions {
    val expectation = DefaultRequestExpectation(requestMatcher)
    expectations.add(expectation)
    return expectation
  }

  fun verify() {
    check(invocationCount == expectations.size) { "Expected request count ${expectations.size} != actual request count $invocationCount" }
  }

  @InternalAPI
  override suspend fun execute(data: HttpRequestData): HttpResponseData {
    val handleScope = MockRequestHandleScope(callContext())
    val expectation = mutex.withLock {
      if (invocationCount >= expectations.size) error("Unhandled ${data.url}")
      val handler = expectations[invocationCount]
      invocationCount += 1
      handler
    }
    expectation.match(handleScope, data)
    return expectation.createResponse(handleScope, data)
  }

  fun reset() {
    expectations.clear()
    invocationCount = 0
  }
}


fun interface RequestMatcher {
  suspend fun match(mockRequestHandleScope: MockRequestHandleScope, request: HttpRequestData)
}

fun interface ResponseCreator {
  suspend fun createResponse(mockRequestHandleScope: MockRequestHandleScope, requestData: HttpRequestData): HttpResponseData
}

fun interface ResponseActions {
  fun andRespond(responseCreator: ResponseCreator)
}

interface RequestExpectation : ResponseActions, RequestMatcher, ResponseCreator

class DefaultRequestExpectation(private val requestMatcher: RequestMatcher) : RequestExpectation {

  private var responseCreator: ResponseCreator? = null

  override fun andRespond(responseCreator: ResponseCreator) {
    this.responseCreator = responseCreator
  }

  override suspend fun match(mockRequestHandleScope: MockRequestHandleScope, request: HttpRequestData) = requestMatcher.match(mockRequestHandleScope, request)

  override suspend fun createResponse(mockRequestHandleScope: MockRequestHandleScope, requestData: HttpRequestData): HttpResponseData {
    return responseCreator?.createResponse(mockRequestHandleScope, requestData) ?: error("andRespond was not called")
  }

}
