package simple.app.demo.ktormock

import io.ktor.client.engine.mock.*
import io.ktor.client.request.*
import io.ktor.http.*
import io.ktor.utils.io.*
import org.assertj.core.api.Assertions
import org.json.JSONObject
import org.skyscreamer.jsonassert.JSONAssert
import org.skyscreamer.jsonassert.JSONCompareMode
import simple.app.demo.serializers.Serializers

fun KtorClientMockEngine.expectGraphQlVariables(variables: Map<String, Any>) = this.expect { _, request ->
  val variablesMap = request.readBody().substringBefore(",\"query\"").substringAfter("\"variables\":")
  JSONAssert.assertEquals(Serializers.json.writeValueAsString(variables), variablesMap, JSONCompareMode.LENIENT)
}

fun ResponseActions.andRespond(body: String) = this.andRespond { scope, _ ->
  scope.respondOk(body)
}

suspend fun HttpRequestData.readBody() = this.body.toByteArray().decodeToString()

fun ResponseActions.andRespondJson(json: Any) = this.andRespond { scope, _ ->
  val serializedObject = Serializers.json.writeValueAsString(json)
  scope.respond(
    ByteReadChannel(serializedObject),
    HttpStatusCode.OK,
    headersOf(HttpHeaders.ContentType, "application/json")
  )
}

fun KtorClientMockEngine.expect(expectedJson: String, expectedUrlParameters: Map<String, AssertUrlParameter>) = this.expect { _, request ->
  expectedUrlParameters.forEach { (key, value) ->
    if (value.shouldBeAssertedAsJson) {
      val serializedValue = JSONObject(value.assertedValue)
      val serializedUrlParam = JSONObject(request.url.parameters[key])
      JSONAssert.assertEquals(serializedValue, serializedUrlParam, JSONCompareMode.STRICT)
    } else {
      Assertions.assertThat(value.assertedValue).isEqualTo(request.url.parameters[key])
    }
  }
  JSONAssert.assertEquals(expectedJson, request.readBody(), JSONCompareMode.LENIENT)
}

data class AssertUrlParameter(
  val shouldBeAssertedAsJson: Boolean,
  val assertedValue: String
)
